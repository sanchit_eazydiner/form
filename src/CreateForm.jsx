import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { MainContainer } from "./components/MainContainer";
import { Form } from "./components/Form";
import { Input } from "./components/Input";
import { PrimaryButton } from "./components/PrimaryButton";

const CreateForm = () => {
    // const { register, handleSubmit, errors } = useForm();
    // const history = useHistory();

    // const onSubmit = (data) => {
    //     history.push("/preview");
    // };

    return (
        <MainContainer>
            <h2>Form</h2>
            <Form>
                <Input
                    // ref={register}
                    id="FullName"
                    name="fullName"
                    type="text"
                    placeholder="fullName"
                    label="fullName"
                />{" "}
                <br />
                <label for="male">Male</label>
                <input type="radio" id="male" name="gender" value="male" />
                <br />
                <label for="female">Female</label>
                <input type="radio" id="female" name="gender" value="female" />
                <br />
                <label for="other">Other</label>
                <input type="radio" id="other" name="gender" value="other" />
                <br />
                <Input
                    // ref={register}
                    id="email"
                    name="email"
                    type="email"
                    placeholder="email"
                    label="email"
                />{" "}
                <br />
                <Input
                    // ref={register}
                    id="phoneNumber"
                    type="tel"
                    label="Phone Number"
                    name="phoneNumber"
                    placeholder="phoneNumber"
                />
                <PrimaryButton type="submit">Preview</PrimaryButton>
            </Form>
        </MainContainer>
    );
};

export default CreateForm;
