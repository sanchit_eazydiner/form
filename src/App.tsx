import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import CreateForm  from './CreateForm.jsx';
import ViewForm from './ViewForm';

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/users/create">Home</Link>
            </li>
            <li>
              <Link to="/users/create">Create</Link>
            </li>
            <li>
              <Link to="/users/view">View</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/users/create">
            {CreateForm}
          </Route>
          <Route path="/users/view">
            {ViewForm}
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
